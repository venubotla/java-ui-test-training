package browserControl;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import utilities.DriverUtils;

public class WebConnector {
	
	public static WebDriver driver;
	
	public static void openBrowser() {
		System.setProperty("webdriver.chrome.driver", DriverUtils.chromeDriverPath);
		System.setProperty("webdriver.gecko.driver", DriverUtils.geckoDriverPath);
		
		if (DriverUtils.browserType.equalsIgnoreCase("firefox")) {
			FirefoxOptions options = new FirefoxOptions();
			options.addArguments("start-maximized");
			driver = new FirefoxDriver(options);
		} else {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized");
			driver = new ChromeDriver(options);
		}
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	public static void closeBrowser() {
		if (driver != null) {
			driver.quit();
		}
	}

}

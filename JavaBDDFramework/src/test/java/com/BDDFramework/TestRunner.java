package com.BDDFramework;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/cucumber.html", "json:target/cucumber.json"} 
				, tags = "@RegressionTest" 
				, features = "src/test/resources/features"
				, glue = {"browserControl", "stepDefinitions"})

public class TestRunner {

}

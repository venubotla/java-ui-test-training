package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import browserControl.WebConnector;

public class DropdownListPageObjects extends WebConnector {
	
	private String dropdownBox = "//select[@id='dropdown']";
	private String dropdownSelectedOption = "//select[@id='dropdown']//option[@selected='selected']";
	
	public WebElement getDropdownBox() {
		return new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(dropdownBox)));
	}
	
	public String getSelectedOption() {
		return new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(dropdownSelectedOption))).getText();
	}

}

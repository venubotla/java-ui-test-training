package stepDefinitions;

import org.junit.Assert;

import browserControl.WebConnector;
import io.cucumber.java.en.Then;

public class AssertionStepdefinitions extends WebConnector {
	
	@Then("I should see {string} text on the page")
	public void i_should_see_text_on_the_page(String expectedText) {
	    String pageSource = driver.getPageSource();
	    Assert.assertTrue(expectedText + " - text not found on the page", pageSource.contains(expectedText));
	}

}

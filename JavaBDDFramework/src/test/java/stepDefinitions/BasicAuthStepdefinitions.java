package stepDefinitions;

import browserControl.WebConnector;
import io.cucumber.java.en.When;
import utilities.DriverUtils;

public class BasicAuthStepdefinitions extends WebConnector {
	
	@When("I authenticate the user with username {string} and password {string}")
	public void i_authenticate_the_user_with_username_and_password(String username, String password) throws InterruptedException {
	    String url = DriverUtils.baseUrl.replace("https://", "https://" + username + ":" + password + "@");
	    driver.get(url + "/basic_auth"); 
	    Thread.sleep(2000);
	}

}

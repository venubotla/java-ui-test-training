package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import browserControl.WebConnector;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utilities.DriverUtils;

public class CommonStepdefinitions extends WebConnector {
	
	@Given("I navigate to {string}")
	public void i_navigate_to(String string) {
	    driver.get(DriverUtils.baseUrl);
	}
	
	@When("I follow {string} link")
	public void i_follow_link(String linkText) {
	    new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.linkText(linkText))).click();
	}
	
	

}

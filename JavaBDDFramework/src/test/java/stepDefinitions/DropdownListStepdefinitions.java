package stepDefinitions;

import org.junit.Assert;

import browserControl.WebConnector;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.DropdownListPageObjects;

public class DropdownListStepdefinitions extends WebConnector {
	
	DropdownListPageObjects dropdownObjects = new DropdownListPageObjects();
	
	
	@When("I select {string} option from the dropdown list")
	public void i_select_option_from_the_dropdown_list(String option) {
	    dropdownObjects.getDropdownBox().sendKeys(option);
	}
	
	@Then("I should see {string} option selected in the dropdown")
	public void i_should_see_option_selected_in_the_dropdown(String option) {
		String selectedOption = dropdownObjects.getSelectedOption();
		Assert.assertEquals(option, selectedOption);
	}

}

package utilities;

public class DriverUtils {
	
	public static String browserType = "chrome";
	public static String baseUrl = "https://the-internet.herokuapp.com";
	
	public static String chromeDriverPath = "../JavaBDDFramework/src/test/java/drivers/chromedriver.exe";
	public static String geckoDriverPath = "../JavaBDDFramework/src/test/java/drivers/geckodriver.exe";

}

Feature: Basic Authentication

  Scenario: Verify Basic Authentication with valid userId and password
    Given I navigate to "/"
    When I follow "Basic Auth" link
      And I authenticate the user with username "admin" and password "admin"
    Then I should see "Congratulations! You must have the proper credentials." text on the page
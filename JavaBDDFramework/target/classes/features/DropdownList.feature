Feature: Dropdown list

  @RegressionTest
  Scenario: Verify that I can select an option from the dropdown list
    Given I navigate to "/"
    When I follow "Dropdown" link
      And I select "Option 1" option from the dropdown list
    Then I should see "Option 1" option selected in the dropdown
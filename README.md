# JavaUIAutomationTrainingTemplate
Java UI test automation template for Test automation training.
The repository contains BasicJava, SeleniumBasics and JavaBDDFramework projects. 
BasicJava project contains java concepts. SeleniumBasics project contains Selenium Webdriver concepts. JavaBDDFramework project contains a BDD framework for end-to-end testing of the web UI.

# Pre-requisites
- Install <a href="https://www.java.com/en/download/win10.jsp" target="_blank">Java</a> and set path
- Download <a href="https://maven.apache.org/download.cgi" target="_blank">Maven</a> and <a href="https://maven.apache.org/install.html" target="_blank">Install</a>
- Download <a href="https://www.eclipse.org/downloads/packages" target="_blank">Eclipse</a> for Java Developers and setup
- Add Maven and Cucumber Eclipse Plugins from Eclipse marketplace
- Familiarise yourself with writing <a href="https://cucumber.io/docs/gherkin" target="_blank">Gherkin Syntax</a> and <a href="https://cucumber.io/docs/cucumber/step-definitions" target="_blank">Step Definitions</a>
- Google Chrome version 86

# Running BDD test scenarios
There are two options available to run the features
## 1. Run scenarios using TestRunner.java
	- Navigate to `src/test/java/com.BDDFramework/TestRunner.java` in Eclipse.
	- Right click on the class and select `Run as -> JUnit Test`.
	This will run the features in default chrome browser. 
## 2. Run scenarios from CLI  
Navigate to the project directory in terminal and run following commands 
### Run scenarios with tag "@RegressionTest"
`mvn clean test -Dcucumber.options="src/test/resources/features" -Dcucumber.filter.tags="@RegressionTest"`